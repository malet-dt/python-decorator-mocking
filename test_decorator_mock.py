import importlib
import unittest
from unittest.mock import patch


def mock_limit_output(_chars: int):
    def limiter(func):
        return lambda text: func(text)

    return limiter


with patch("limit_output_decorator.limit_output", mock_limit_output):
    from decorated_function import uppercase


class TestADecoratorMockUsingImportFrom(unittest.TestCase):
    def test_with_decorator_mock(self):
        # It's only possible to test the undecorated or decorated function using `from X import Y`
        # As importlib doesn't support reloading in that format
        self.assertEqual(uppercase("A very long message"), "A VERY LONG MESSAGE")


# This is the only way to test both decorated and undecorated functions
class TestBDecoratorMockUsingImportlibReload(unittest.TestCase):
    import decorated_function

    @patch("limit_output_decorator.limit_output", mock_limit_output)
    def test_with_decorator_mock(self):
        importlib.reload(self.decorated_function)

        self.assertEqual(
            self.decorated_function.uppercase("A very long message"),
            "A VERY LONG MESSAGE",
        )

    def test_with_original_decorator(self):
        importlib.reload(self.decorated_function)

        self.assertEqual(
            self.decorated_function.uppercase("A very long message"), "A VERY LON"
        )
