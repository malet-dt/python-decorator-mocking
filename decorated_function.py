from limit_output_decorator import limit_output


@limit_output(10)
def uppercase(text: str):
    return text.upper()
