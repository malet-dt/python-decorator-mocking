def limit_output(chars: int):
    def limiter(func):
        return lambda text: func(text)[:chars]

    return limiter
